
// Using compiler flags for nice tricks

#[derive(Debug)]
struct Student {
    name : String,
    roll : u16
}


fn main() {
    let _student = Student {
        name:"Swarnim".to_string(),
        roll:13
    };
    println!("{:?}",_student);
}
