


// Let's try understanding basic variables here


// struct as type
struct foo {
    name : &[u8],  // this is a slice can also be written as &[u8]
    address : String, // this is a string stored as a Vec<u8>
    number : u64,  // all of 9 digit numbers
    marks : i8,
    percentage : f32 // this is a float
    // f16  -- this doesn't exist cause it's not supported on large number of platforms
}


fn main () {
    // let variable_name : optionally_type = initialzation_value;

    // creating an instance
    let my_foo = foo {
        name : "Swarnim",
        address : "Some Place in India",
        number : 7588679331,
        marks : 200,
        percentage : 90.78
    };


    // using print statement
    // println!("print something");

}