

// borrowing and ownership of stuff

/// EG 1

// fn main() {
//     let mut s = String::from("hello");

//     let r1 = &s; 
//     let r2 = &s; 
//     let r3 = &mut s;

//     println!("{}, {}, and {}", r1, r2, r3);
// }


/// EG 2

// fn main () {
//     let mut my_vec : Vec<u8> = vec![1,2,3,4];
    
//     let a_vec = &mut my_vec;
//     add_to_vec(&mut my_vec);

//     println!("{:?} \n :-> {:?}", my_vec, a_vec);
// }

// fn add_to_vec(vec : &mut Vec<u8>) {
//     ()
// }