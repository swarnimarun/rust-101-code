

// using simple functions

/// EG 1

// fn my_func() {
//     println!("my_func was called");
// }

// fn main() {
//     my_func();
// }

/// EG 2

fn my_func(var1 : u8, var2 : f32) -> u8 {
    if var1 > var2 {
        16
    } else {
        32
    }
}

fn main() {
    println!("{}", my_func());
}