
#include <iostream>
#include <vector>

void do_something(std::vector<int> &arr) {
    arr.push_back(10);
    arr.push_back(20);
    arr.push_back(30);
    arr.push_back(40);
}

int main() {
    std::vector<int> array;
    array.push_back(32);

    auto zeroth = &array[0];

    //do_something(array);

    std::cout << *zeroth << std::endl;

}